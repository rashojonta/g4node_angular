import { Component } from '@angular/core';
import { Customer } from './customer.entity';
import { HttpClient } from "@angular/common/http";
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'JS';
  filteredCustomers: Array<Customer> = [];
  customers: Array<Customer> = [];
  searchValue:string;
  selectedCustomer:Customer;
  

  constructor(private httpClient: HttpClient) { }



  search = (event: any) => {
    if(event.keyCode == 13){
      
        if(!this.searchValue || this.searchValue.length == 0){
          this.filteredCustomers = this.customers;
        }else{
          this.filteredCustomers = this.customers.filter(x => 
            x.first_name.toLowerCase() == this.searchValue.toLowerCase() ||
            x.last_name.toLowerCase() == this.searchValue.toLowerCase() ||
            x.email.toLowerCase() == this.searchValue.toLowerCase() 
            
            );
        }

    }

  }

  
  
  ngOnInit() {
    this.httpClient.get("assets/customers.json").subscribe(data => {
      this.customers = this.filteredCustomers = data as Array<Customer>;


    })



  }
}