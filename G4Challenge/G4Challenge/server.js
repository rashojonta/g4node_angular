const express = require('express');
const mysql = require('mysql');
const app = express();
var path = require('path');
var bodyParser = require('body-parser')


// parse application/json
app.use(bodyParser.json())

app.use(function(req, res, next) {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
  next();
});
app.set('port', process.env.PORT || 3001);


app.use(express.static('public'));

  // Return the main index.html, so react-router render the route in the client
  app.get('/', (req, res) => {
    res.sendFile(path.resolve('public', 'index.html'));
  });


const host = 'localhost';
const user = 'root';
const pswd = 'Rashoj21#';
const dbname = 'g4.challenge';

// config db ====================================
const pool = mysql.createPool({
  host: host,
  user: user,
  password: pswd,
  port: '3306',
  database: dbname
});


app.get('/api/customers', (req, res) => {
   let queryString = `SELECT * from customers`;
   if(req.query.searchValue){
     queryString= `SELECT * from customers where first_name =? OR email =?`
   }
   console.log(queryString);
   pool.query(queryString,[req.query.searchValue,req.query.searchValue], function(err, rows, fields) {
    if (err) throw err;
    console.log("Rows length"+ rows.length);
    if (rows.length > 0) {
      res.json(rows);
    } else {
      res.json([]);
    }
  });

});

app.post('/api/customers/create', (req, res) => {

  try {
    
    let sql = "INSERT INTO customers (email,first_name,last_name,ip,latitude,longitude,created_at) VALUES (?,?,?,?,?,?,?)";
    console.log(sql);
    let ip = req.header('x-forwarded-for') || req.connection.remoteAddress;
    let val = [req.body.email,req.body.firstName,req.body.lastName,ip,req.body.latitude,req.body.longitude,new Date()];
    
    
    

    pool.query(sql, val, function(err, result) {
      if (err) { 
          
            throw err;

        
      }else {

         return res.status(200).send();
      
  
      }


    });

  } catch (error) {
    res.status(400).send(error);
  }
 


  


});

app.put('/api/customers/update', (req, res) => {

  try {
    
    // let sql = "INSERT INTO customers (email,first_name,last_name,ip,latitude,longitude,created_at) VALUES (?,?,?,?,?,?,?)";
    // console.log(sql);

    // update statment
   let sql = `UPDATE customers SET email =? , first_name =?, last_name =? , ip =?, latitude =?, longitude =?, updated_at =? WHERE id = ?`;
    let ip = req.header('x-forwarded-for') || req.connection.remoteAddress;
    let val = [req.body.email,req.body.firstName,req.body.lastName,ip,req.body.latitude,req.body.longitude,new Date(), req.body.id];
    
    
    

    pool.query(sql, val, function(err, result) {
      if (err) { 
          
            throw err;

        
      }else {

         return res.status(200).send();
      
  
      }


    });

  } catch (error) {
    res.status(400).send(error);
  }
 


  


});






app.listen(app.get('port'), () => {
  console.log(`Find the server at: http://localhost:${app.get('port')}/`); // eslint-disable-line no-console
});
